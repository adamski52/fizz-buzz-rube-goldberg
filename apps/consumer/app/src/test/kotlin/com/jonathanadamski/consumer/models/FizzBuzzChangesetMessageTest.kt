package com.jonathanadamski.consumer.models

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class FizzBuzzChangesetMessageTest {
    lateinit var json: String
    val objectMapper = jacksonObjectMapper()

    @BeforeEach
    fun onBeforeEach() {
        json = "{\"_id\":{\"_data\":\"8260996BB7000000012B022C0100296E5A1004BE208B099BCF4106822DE274B0B9D39A46645F6964006460996BB7425E6C30F60E44810004\"},\"operationType\":\"insert\",\"clusterTime\":{\"\$timestamp\":{\"t\":1620667319,\"i\":1}},\"fullDocument\":{\"_id\":{\"\$oid\":\"60996bb7425e6c30f60e4481\"},\"uuid\":\"ebe3b2a2-b59c-4fe8-b2af-9c9372799e80\",\"endingNumber\":10},\"ns\":{\"db\":\"fizzbuzz\",\"coll\":\"messages\"},\"documentKey\":{\"_id\":{\"\$oid\":\"60996bb7425e6c30f60e4481\"}}}";
    }

    @Test
    fun it_should_convertBackAndForth() {
        val actual = objectMapper.readValue(json, FizzBuzzChangesetMessage::class.java)
        val expected = objectMapper.writeValueAsString(actual);

        assertEquals(expected, json)
    }
}
