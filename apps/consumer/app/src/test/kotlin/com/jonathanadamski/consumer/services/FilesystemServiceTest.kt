package com.jonathanadamski.consumer.services

import com.jonathanadamski.consumer.models.FizzBuzzFilesystemResponse
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.io.File

@SpringBootTest(
    properties = ["filesystem.dir=/tmp/wat/"]
)
class FilesystemServiceTest {

    @Autowired
    lateinit var filesystemService: FilesystemService

    val dir = "/tmp/wat/"
    val file = "my-file"
    val extension = ".txt"
    val filepath = "$dir$file$extension"
    val contents = "lolwat"

    @Test
    fun writeFile_should_writeToAFile() {
        filesystemService.writeToFile(file, contents)
        assertEquals(true, File(filepath).exists())
        assertEquals(contents, File(filepath).readText())

        File(filepath).delete()
        assertEquals(false, File(filepath).exists())
    }

    @Test
    fun getContents_should_gimmeAMap() {
        val file2 = file + "2"
        val filepath2 = "$dir$file2$extension"

        filesystemService.writeToFile(file, contents)
        filesystemService.writeToFile(file2, contents)

        val actual = filesystemService.getDirectoryContents()

        val expected = ArrayList<FizzBuzzFilesystemResponse>()
        expected.add(FizzBuzzFilesystemResponse(file, contents))
        expected.add(FizzBuzzFilesystemResponse(file2, contents))
        assertEquals(expected, actual)

        assertEquals(true, File(filepath).exists())
        assertEquals(true, File(filepath2).exists())

        File(filepath).delete()
        File(filepath2).delete()
    }

    @Test
    fun deleteFile_should_clobber() {
        filesystemService.writeToFile(file, contents)

        assertEquals(true, File(filepath).exists())

        filesystemService.deleteFile(file)

        assertEquals(false, File(filepath).exists())
    }
}
