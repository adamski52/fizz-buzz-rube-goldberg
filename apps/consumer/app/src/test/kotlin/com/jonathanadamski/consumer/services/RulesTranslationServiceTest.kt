package com.jonathanadamski.consumer.services

import com.jonathanadamski.consumer.models.FizzBuzzRule
import com.jonathanadamski.consumer.services.RulesTranslationService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class RulesTranslationServiceTest {

    lateinit var rulesTranslationService: RulesTranslationService

    @BeforeEach
    fun onBeforeEach() {
        rulesTranslationService = RulesTranslationService()
    }

    @Test
    fun it_should_workGivenWeirdRules() {
        val rule1 = FizzBuzzRule(1, 2, "hello")
        val rule2 = FizzBuzzRule(2, 3, "goodbye")
        val rules = listOf(rule1, rule2)

        val actual = rulesTranslationService.getResultForValue(1,10, rules)
        assertEquals("1 hello goodbye hello 5 hello-goodbye 7 hello goodbye hello", actual)
    }

    @Test
    fun it_should_workGivenNormalRules() {
        val rule1 = FizzBuzzRule(1, 3, "hello")
        val rule2 = FizzBuzzRule(2, 5, "goodbye")
        val rules = listOf(rule1, rule2)

        val actual = rulesTranslationService.getResultForValue(1,10, rules)
        assertEquals("1 2 hello 4 goodbye hello 7 8 hello goodbye", actual)
    }
}
