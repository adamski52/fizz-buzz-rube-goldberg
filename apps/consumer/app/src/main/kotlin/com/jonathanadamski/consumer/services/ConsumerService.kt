package com.jonathanadamski.consumer.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service

@Service
class ConsumerService {

    @Autowired
    lateinit var messageHandlerService: MessageHandlerService

    @KafkaListener(groupId = "fizzbuzz", topics = ["#{'\${kafka.topics}'.split(',')}"])
    fun listen(message:String) : Unit {
        val result = messageHandlerService.handleMessage(message)
        println("message: $message | result: $result")
    }
}
