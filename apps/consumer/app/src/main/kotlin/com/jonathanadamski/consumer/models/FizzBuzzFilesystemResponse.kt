package com.jonathanadamski.consumer.models


data class FizzBuzzFilesystemResponse (
    val uuid: String,
    val contents: String
)
