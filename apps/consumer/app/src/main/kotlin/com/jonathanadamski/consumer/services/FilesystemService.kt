package com.jonathanadamski.consumer.services

import com.jonathanadamski.consumer.models.FizzBuzzFilesystemResponse
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.File

@Service
class FilesystemService {
    @Value("\${filesystem.dir}")
    var dir = "/tmp/messages/"

    fun writeToFile(fileNameWithoutExtension:String, contents:String) {
        val path = "$dir$fileNameWithoutExtension.txt"
        File(path).also {
            file -> file.parentFile.mkdirs()
        }.writeText(contents)
    }

    fun getDirectoryContents():ArrayList<FizzBuzzFilesystemResponse> {
        var result = ArrayList<FizzBuzzFilesystemResponse>()

        File(dir).walk().filter {
            it.isFile
        }.forEach {
            result.add(FizzBuzzFilesystemResponse(it.nameWithoutExtension, it.readText()))
        }

        return result
    }

    fun deleteFile(file: String) {
        val path = "$dir$file.txt"
        File(path).delete()
    }
}
