package com.jonathanadamski.consumer.models

import com.fasterxml.jackson.annotation.JsonProperty

data class FizzBuzzChangesetMessageId (
    @JsonProperty("_data")
    val data: String
)

data class FizzBuzzChangesetMessageTimestamp (
    val t: Long,
    val i: Int
)

data class FizzBuzzChangesetMessageClusterTime (
    @JsonProperty("\$timestamp")
    val timestamp: FizzBuzzChangesetMessageTimestamp
)

data class FizzBuzzChangesetOid (
    @JsonProperty("\$oid")
    val oid: String
)

data class FizzBuzzChangesetMessageFullDocument (
    @JsonProperty("_id")
    val id: FizzBuzzChangesetOid,
    val uuid: String,
    val endingNumber: Int
)

data class FizzBuzzChangesetMessageNS (
    val db: String,
    val coll: String
)

data class FizzBuzzChangesetDocumentKey (
    @JsonProperty("_id")
    val id: FizzBuzzChangesetOid
)

data class FizzBuzzChangesetMessage (
    @JsonProperty("_id")
    val id: FizzBuzzChangesetMessageId,
    val operationType: String,
    val clusterTime: FizzBuzzChangesetMessageClusterTime,
    val fullDocument: FizzBuzzChangesetMessageFullDocument,
    val ns: FizzBuzzChangesetMessageNS,
    val documentKey: FizzBuzzChangesetDocumentKey
)
