package com.jonathanadamski.consumer.models

import javax.persistence.*

@Entity
@Table(name = "rules")
data class FizzBuzzRule (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int,

    @Column(nullable = false, unique = true)
    val value: Int,

    @Column(nullable = false)
    val substituteWith: String
)
