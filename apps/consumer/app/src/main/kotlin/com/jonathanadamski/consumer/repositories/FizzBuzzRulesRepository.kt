package com.jonathanadamski.consumer.repositories

import com.jonathanadamski.consumer.models.FizzBuzzRule
import org.springframework.data.jpa.repository.JpaRepository
import javax.transaction.Transactional

@Transactional(Transactional.TxType.MANDATORY)
interface FizzBuzzRulesRepository : JpaRepository <FizzBuzzRule, Int>
