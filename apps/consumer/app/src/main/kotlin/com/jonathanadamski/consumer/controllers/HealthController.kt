package com.jonathanadamski.consumer.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class HealthController {
    @GetMapping("/health")
    @ResponseStatus(value = HttpStatus.OK)
    fun getHealth(): String {
        return "OK"
    }
}
