package com.jonathanadamski.consumer.services

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper;
import com.jonathanadamski.consumer.models.FizzBuzzChangesetMessage

@Service
class MessageHandlerService {
    @Autowired
    lateinit var rulesTranslationService: RulesTranslationService

    @Autowired
    lateinit var databaseRulesService: DatabaseRulesService

    @Autowired
    lateinit var filesystemService: FilesystemService

    val objectMapper = jacksonObjectMapper()

    fun handleMessage(message:String):String {
        val changesetMessage = objectMapper.readValue(message, FizzBuzzChangesetMessage::class.java)
        val rules = databaseRulesService.getAll()
        val result = rulesTranslationService.getResultForValue(1, changesetMessage.fullDocument.endingNumber, rules)

        filesystemService.writeToFile(changesetMessage.fullDocument.uuid, result)

        return result
    }
}
