package com.jonathanadamski.consumer.services

import com.jonathanadamski.consumer.models.FizzBuzzRule
import com.jonathanadamski.consumer.repositories.FizzBuzzRulesRepository
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class DatabaseRulesService(val repo: FizzBuzzRulesRepository) {
    fun getAll(): List<FizzBuzzRule> {
        return repo.findAll();
    }
}
