package com.jonathanadamski.consumer.services

import com.jonathanadamski.consumer.models.FizzBuzzRule
import org.springframework.stereotype.Service

@Service
class RulesTranslationService {
    fun getResultForValue(startValue: Int, maxValue: Int, rules: List<FizzBuzzRule>): String {
        val result: MutableList<String> = mutableListOf()

        for (i in startValue..maxValue) {
            result.add(getValueFromRule(i, rules))
        }

        return result.joinToString(" ")
    }

    private fun getValueFromRule(value: Int, rules: List<FizzBuzzRule>): String {
        val matches = rules.filter {value % it.value == 0}
        if(matches.isEmpty()) {
            return value.toString()
        }

        return matches.map {match -> match.substituteWith }.joinToString( "-")
    }
}
