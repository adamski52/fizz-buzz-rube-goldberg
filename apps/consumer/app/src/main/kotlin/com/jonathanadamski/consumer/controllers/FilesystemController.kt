package com.jonathanadamski.consumer.controllers

import com.jonathanadamski.consumer.models.FizzBuzzFilesystemResponse
import com.jonathanadamski.consumer.services.FilesystemService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
class FilesystemController {

    @Autowired
    lateinit var filesystemService: FilesystemService

    @GetMapping("/filesystem")
    fun getFiles():ArrayList<FizzBuzzFilesystemResponse> {
        return filesystemService.getDirectoryContents()
    }

    @DeleteMapping("/filesystem/{filename}")
    fun deleteFile(@PathVariable filename: String) {
        return filesystemService.deleteFile(filename)
    }
}
