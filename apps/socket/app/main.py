from flask import Flask, request
from flask_socketio import SocketIO

import os
import json

SECRET_KEY = os.getenv("SECRET_KEY")
MAX_HEALTHCHECK_FAILS = 3
TO_CLIENT_EVENT = "toClient"
FIZZ_BUZZ_ENDPOINT = "/fizzbuzz"
HEALTHCHECK_EVENT = "healthcheck"

app = Flask(__name__)
app.config["SECRET_KEY"] = SECRET_KEY

num_healthcheck_fails = 0

socket_server = SocketIO(app, async_mode=None)

@app.route(FIZZ_BUZZ_ENDPOINT, methods = ["POST"])
def handle_input():
    try:
        socket_server.emit(TO_CLIENT_EVENT, {
            "uuid": request.json["uuid"],
            "message": request.json["message"]
        })
    except Exception as err:
        app.logger.error(err)

        return json.dumps({
            "success": False
        }), 400, {
            "Content-Type": "application/json"
        }

    return json.dumps({
        "success": True
    }), 200, {
        "Content-Type": "application/json"
    }


@app.route("/health")
def handle_health_request():
    global num_healthcheck_fails

    try:
        num_healthcheck_fails += 1
        socket_client = socket_server.test_client(app)
        socket_client.emit(HEALTHCHECK_EVENT)
    except Exception:
        app.logger.info("Failed to send healthcheck message (" + str(num_healthcheck_fails) + "x).  Pod may be unhealthy.")

    if num_healthcheck_fails > MAX_HEALTHCHECK_FAILS:
        return ("Number of health failures (" + str(num_healthcheck_fails) + ") exceeds maximum (" + str(MAX_HEALTHCHECK_FAILS) + ")", 500)

    return ("OK", 200)


@socket_server.on(HEALTHCHECK_EVENT)
def handle_health_message():
    global num_healthcheck_fails
    num_healthcheck_fails = 0


if __name__ == "__main__":
    socket_server.run(app, host="0.0.0.0", debug=True)
