package com.jonathanadamski.controllers;

import com.jonathanadamski.kafka.KafkaProducerService;
import com.jonathanadamski.models.FizzBuzzMessage;
import com.jonathanadamski.models.FizzBuzzPayload;
import com.jonathanadamski.models.FizzBuzzResponse;
import com.jonathanadamski.services.MessageService;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.apache.kafka.clients.producer.Producer;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@MicronautTest
public class ProducerControllerTest {
    @Inject
    private ProducerController producerController;

    private Producer<String, String> mockProducer;
    private final String mockTopic = "loltopic";
    private final String testString = "wat";

    @MockBean(MessageService.class)
    MessageService producerService() {
        MessageService messageService = Mockito.mock(MessageService.class);
        when(messageService.send(eq(mockProducer), eq(mockTopic), any(FizzBuzzMessage.class))).thenReturn(testString);
        return messageService;
    }

    @MockBean(KafkaProducerService.class)
    KafkaProducerService kafkaProducerService() {
        mockProducer = Mockito.mock(Producer.class);

        KafkaProducerService kafkaProducerService = Mockito.mock(KafkaProducerService.class);
        doNothing().when(kafkaProducerService).setup();
        when(kafkaProducerService.getProducer()).thenReturn(mockProducer);
        when(kafkaProducerService.getTopic()).thenReturn(mockTopic);
        return kafkaProducerService;
    }

    @Test
    public void fizzBuzzEndpoint_should_sendCallServiceWithPayload() {
        FizzBuzzPayload payload = new FizzBuzzPayload();
        payload.setEndingNumber(10);
        FizzBuzzResponse actual = producerController.post(payload);

        FizzBuzzResponse expected = new FizzBuzzResponse();
        expected.setUuid(testString);
        assertEquals(expected, actual);
    }
}
