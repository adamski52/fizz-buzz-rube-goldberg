package com.jonathanadamski;

import com.jonathanadamski.kafka.KafkaProducerService;
import io.micronaut.runtime.EmbeddedApplication;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import javax.inject.Inject;

import static org.mockito.Mockito.doNothing;

@MicronautTest
class ApplicationTest {

    @Inject
    EmbeddedApplication<?> application;

    @MockBean(KafkaProducerService.class)
    KafkaProducerService kafkaProducerService() {
        KafkaProducerService kafkaProducerService = Mockito.mock(KafkaProducerService.class);
        doNothing().when(kafkaProducerService).setup();
        return kafkaProducerService;
    }

    @Test
    void testItWorks() {
        Assertions.assertTrue(application.isRunning());
    }

}
