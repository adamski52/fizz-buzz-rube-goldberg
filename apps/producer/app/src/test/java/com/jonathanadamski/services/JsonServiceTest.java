package com.jonathanadamski.services;

import com.jonathanadamski.kafka.KafkaProducerService;
import com.jonathanadamski.models.FizzBuzzMessage;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;

@MicronautTest
public class JsonServiceTest {

    @Inject
    private JsonService jsonService;

    private final int mockEndingNumber = 10;
    private final String mockUUID = "b3b56b72-7b2d-4649-a424-25b54ede6c63";

    @MockBean(KafkaProducerService.class)
    KafkaProducerService kafkaProducerService() {
        KafkaProducerService kafkaProducerService = Mockito.mock(KafkaProducerService.class);
        doNothing().when(kafkaProducerService).setup();
        return kafkaProducerService;
    }

    @Test
    public void getJson_should_work() {
        FizzBuzzMessage fizzBuzzMessage = new FizzBuzzMessage();
        fizzBuzzMessage.setEndingNumber(mockEndingNumber);
        fizzBuzzMessage.setUuid(mockUUID);

        String actual = jsonService.getJson(fizzBuzzMessage);
        String expected = "{\"uuid\":\"" + mockUUID +"\",\"endingNumber\":" + mockEndingNumber + "}";
        assertEquals(expected, actual);
    }
}
