package com.jonathanadamski.services;

import com.jonathanadamski.kafka.KafkaProducerService;
import com.jonathanadamski.models.FizzBuzzMessage;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;

@MicronautTest
public class MessageServiceTest {

    @Inject
    private MessageService messageService;

    @Inject
    private JsonService jsonService;

    private final int mockEndingNumber = 10;
    private final String mockTopic = "loltopic";
    private final String mockUUID = "b3b56b72-7b2d-4649-a424-25b54ede6c63";

    private FizzBuzzMessage mockMessage;
    private String mockJson;

    @MockBean(KafkaProducerService.class)
    KafkaProducerService kafkaProducerService() {
        KafkaProducerService kafkaProducerService = Mockito.mock(KafkaProducerService.class);
        doNothing().when(kafkaProducerService).setup();
        return kafkaProducerService;
    }

    @BeforeEach
    public void onBeforeEach() {
        mockMessage = new FizzBuzzMessage();
        mockMessage.setEndingNumber(mockEndingNumber);
        mockMessage.setUuid(mockUUID);

        mockJson = jsonService.getJson(mockMessage);
    }

    @Test
    public void send_should_freakOutIfNoProducer() {
        assertThrows(Exception.class, () -> {
            messageService.send(null, mockTopic, mockMessage);
        });
    }

    @Test
    public void send_should_work() {
        Producer<String, String> mockProducer = Mockito.mock(Producer.class);

        messageService.send(mockProducer, mockTopic, mockMessage);

        ProducerRecord producerRecord = new ProducerRecord<>(mockTopic, mockUUID, mockJson);
        verify(mockProducer, times(1)).send(eq(producerRecord));
    }
}
