package com.jonathanadamski.services;

import com.jonathanadamski.models.FizzBuzzMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@Slf4j
public class MessageService {

    @Inject
    private JsonService jsonService;

    public String send(Producer<String, String> producer, String topic, FizzBuzzMessage message) {

        String key = message.getUuid();
        String json = jsonService.getJson(message);

        try {
            producer.send(new ProducerRecord<>(topic, key, json));
            log.info("Message sent ({}): {}: {}", topic, key, json);
            return key;
        }
        catch(Exception e) {
            log.error("Failed sending message ({}): {}: {}", topic, key, json);
            throw e;
        }
    }



        // consumer
        //Properties config = new Properties();
        //config.put("client.id", InetAddress.getLocalHost().getHostName());
        //config.put("group.id", "foo");
        //config.put("bootstrap.servers", "host1:9092,host2:9092");
        //new KafkaConsumer<K, V>(config);
//        while (running) {
//            ConsumerRecords<K, V> records = consumer.poll(Long.MAX_VALUE);
//            process(records); // application-specific processing
//            consumer.commitSync();
//        }
}
