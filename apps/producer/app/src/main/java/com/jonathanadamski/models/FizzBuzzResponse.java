package com.jonathanadamski.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class FizzBuzzResponse implements Serializable {
    private String uuid;
}
