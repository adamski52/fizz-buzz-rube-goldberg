package com.jonathanadamski;

import com.jonathanadamski.kafka.KafkaProducerService;
import io.micronaut.context.event.ApplicationEventListener;
import io.micronaut.runtime.Micronaut;
import io.micronaut.runtime.server.event.ServerStartupEvent;

import javax.inject.Inject;

public class Application implements ApplicationEventListener<ServerStartupEvent> {

    @Inject
    private KafkaProducerService kafkaProducerService;

    public static void main(String[] args) {
        Micronaut.run(Application.class, args);
    }

    @Override
    public void onApplicationEvent(ServerStartupEvent e) {
        kafkaProducerService.setup();
    }
}
