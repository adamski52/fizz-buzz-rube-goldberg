package com.jonathanadamski.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class FizzBuzzPayload implements Serializable {
    private int endingNumber;
}
