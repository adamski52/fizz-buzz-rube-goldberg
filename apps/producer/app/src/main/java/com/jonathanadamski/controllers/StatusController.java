package com.jonathanadamski.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller("/")
public class StatusController {

    @Get("/health")
    public HttpResponse<String> health() {
        return HttpResponse.ok();
    }

    @Get("/ready")
    public HttpResponse<String> ready() {
        return HttpResponse.ok();
    }
}
