package com.jonathanadamski.kafka;

import io.micronaut.context.annotation.Value;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;

import javax.inject.Singleton;
import java.util.Properties;

@Singleton
@Slf4j
@Data
public class KafkaProducerService {
    @Value("${kafka.bootstrap.servers}")
    private String kafkaBootstrapServers;

    @Value("${kafka.topic}")
    private String topic;

    private Producer<String, String> producer;

    public void setup() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", kafkaBootstrapServers);
        properties.put("acks", "all");
        properties.put("retries", 0);
        properties.put("batch.size", 16384);
        properties.put("linger.ms", 1);
        properties.put("buffer.memory", 33554432);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        try {
            producer = new KafkaProducer<>(properties);
            log.info("Created Producer with properties: {}", properties);
        }
        catch(Exception e) {
            log.error("Error creating producer with properties: {}", properties);
            throw e;
        }
    }
}
