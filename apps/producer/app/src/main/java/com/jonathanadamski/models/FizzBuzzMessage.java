package com.jonathanadamski.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
public class FizzBuzzMessage implements Serializable {
    private String uuid = UUID.randomUUID().toString();
    private int endingNumber;
}
