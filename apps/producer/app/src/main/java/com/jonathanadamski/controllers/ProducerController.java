package com.jonathanadamski.controllers;

import com.jonathanadamski.kafka.KafkaProducerService;
import com.jonathanadamski.models.FizzBuzzMessage;
import com.jonathanadamski.models.FizzBuzzResponse;
import com.jonathanadamski.services.MessageService;
import com.jonathanadamski.models.FizzBuzzPayload;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import lombok.extern.slf4j.Slf4j;
import javax.inject.Inject;

@Slf4j
@Controller("/api/fizz-buzz")
public class ProducerController {

    @Inject
    private KafkaProducerService kafkaProducerService;

    @Inject
    private MessageService messageService;

    @Post
    public FizzBuzzResponse post(@Body FizzBuzzPayload payload) {
        FizzBuzzMessage fizzBuzzMessage = new FizzBuzzMessage();
        fizzBuzzMessage.setEndingNumber(payload.getEndingNumber());

        String topic = kafkaProducerService.getTopic();

        String uuid = messageService.send(kafkaProducerService.getProducer(), topic, fizzBuzzMessage);

        FizzBuzzResponse response = new FizzBuzzResponse();
        response.setUuid(uuid);

        return response;
    }
}
