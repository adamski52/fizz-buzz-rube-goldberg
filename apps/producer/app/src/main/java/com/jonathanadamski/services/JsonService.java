package com.jonathanadamski.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jonathanadamski.models.FizzBuzzMessage;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@Singleton
@Slf4j
public class JsonService {
    private final ObjectMapper objectMapper = new ObjectMapper();

    public String getJson(FizzBuzzMessage message) {
        try {
            return objectMapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            log.error("Failed to create JSON for message.  Using nonsense.  Message was: {}", message);
            return "{\"uuid\": null, \"message\": \"\"}";
        }
    }
}
