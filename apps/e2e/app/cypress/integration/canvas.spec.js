context("Canvas", () => {
    beforeEach(() => {
      cy.visit("http://localhost:3000");
    });

    it("should be blank by default", () => {
      cy.get(".number-input").clear().type("10");
      cy.get(".go-button").click();

      cy.get(".number-input").clear().type("9");
      cy.get(".go-button").click();

      cy.get(".number-input").clear().type("8");
      cy.get(".go-button").click();

      cy.get(".number-input").clear().type("7");
      cy.get(".go-button").click();
      
      cy.wait(120000); // our ticker is about 1 minute.  give some wiggles.
      
      cy.get("canvas").toMatchImageSnapshot();
    });
});