context("Input Box", () => {
    beforeEach(() => {
      cy.visit("http://localhost:3000");
    });

    it("should have a default", () => {
      cy.get(".number-input").should("have.value", "10");
    });

    it("should change value if value is a number", () => {
      cy.get(".number-input").clear().type("a1b2c3").should("have.value", "123");
    });

    it("should not change value if value is not a number", () => {
      cy.get(".number-input").type("crap").should("have.value", "10");
    });
});