context("Go Button", () => {
    beforeEach(() => {
      cy.visit("http://localhost:3000");
    });

    it("should say go", () => {
      cy.get(".go-button").should("have.text", "Go");
    });
});