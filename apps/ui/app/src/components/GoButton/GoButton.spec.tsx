import React from "react";
import GoButton from "./GoButton";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";

describe("GoButton", () => {
    let onClick = jest.fn(),
        container: HTMLDivElement,
        defaultValue: number;

    beforeEach(() => {
        container = document.createElement("div");
        document.body.appendChild(container);
        defaultValue = 10;
    });

    afterEach(() => {
        document.body.removeChild(container);
    });

    it("should have a button that says 'Go'", () => {
        act(() => {
            ReactDOM.render(<GoButton value={defaultValue} onClick={onClick} />, container);
        });

        const button = container.querySelector("button");
        expect(button!.textContent).toBe("Go");
    });

    it("should call onClick when button clicked", () => {
        act(() => {
            ReactDOM.render(<GoButton value={defaultValue} onClick={onClick} />, container);
        });

        const button = container.querySelector("button");
        act(() => {
            button!.dispatchEvent(new MouseEvent("click", {
                bubbles: true
            }));
        });

        expect(onClick).toHaveBeenCalledWith(defaultValue);
    });

    it("should have a number picker with the default value", () => {
        act(() => {
            ReactDOM.render(<GoButton value={defaultValue} onClick={onClick} />, container);
        });

        const input = container.querySelector("input");
        expect(input!.value).toBe(defaultValue + "");
    });
});
