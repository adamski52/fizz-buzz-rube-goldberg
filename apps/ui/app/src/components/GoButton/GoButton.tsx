import React, { ChangeEvent, MouseEvent } from "react";
import { Button } from "@blueprintjs/core";
import "./GoButton.scss";

export interface IGoButtonProps {
    onClick: (value:number) => void;
    value: number;
}

export interface IGoButtonState {
    value: number;
}

export default class GoButton extends React.Component<IGoButtonProps, IGoButtonState> {
    constructor(props:IGoButtonProps) {
        super(props);

        this.state = {
            value: props.value || 10 // no negatives, no 0's
        };

        this.onChange = this.onChange.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    public render() {
        return (
            <div className="go-button-container">
                <h1>Gimme Fizz-Buzz up to...</h1>
                <input type="number" defaultValue={this.state.value} onChange={this.onChange} className="number-input" />
                <Button text="Go" onClick={this.onClick} className="go-button" />
            </div>
        );
    }

    private onChange(e:ChangeEvent<HTMLInputElement>) {
        let value = parseInt(e.currentTarget.value || "10", 10);
        
        this.setState({
            value
        });
    }

    private onClick(e:MouseEvent<HTMLElement>) {
        e.preventDefault();
        this.props.onClick(this.state.value);
    }
}