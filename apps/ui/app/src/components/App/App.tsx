import React from "react";
import "./App.scss";
import Stage from "../Stage/Stage";
import GoButton from "../GoButton/GoButton";
import HttpService from "../../services/HttpService";
import SocketService, { ISocketRequestResponse } from "../../services/SocketService";

export interface IAppProps {
    httpService:HttpService;
    socketService:SocketService;
    removeDelayMs: number;
}

export interface IAppState {}

export default class App extends React.Component<IAppProps, IAppState> {
    constructor(props:IAppProps) {
        super(props);

        this.onGo = this.onGo.bind(this);
    }

    public render() {
        return (
            <div className="app-container">
                <GoButton onClick={this.onGo} value={10} />
                <Stage socketService={this.props.socketService} removeDelayMs={this.props.removeDelayMs} textSize={12} />
            </div>
        );
    }

    private async onGo(value: number) {
        try {
            let response:ISocketRequestResponse = await this.props.httpService.post("/api/fizz-buzz", {
                endingNumber: value
            });

            this.props.socketService.keep(response.uuid);
        }
        catch(e) {
            console.error(e);
        }
    }
}
