import Stage from "./Stage";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";
import SocketService from "../../services/SocketService";
import { io } from "socket.io-client";

describe("Stage", () => {
    jest.mock("socket.io-client");
    
    let container: HTMLDivElement;
    let socketService:SocketService = new SocketService(io());

    beforeEach(() => {
        container = document.createElement("div");
        document.body.appendChild(container);
    });

    afterEach(() => {
        document.body.removeChild(container);
    });

    it("should render a canvas into the 'stage' element", () => {
        act(() => {
            ReactDOM.render(<Stage socketService={socketService} removeDelayMs={20} textSize={20} />, container);
        });

        const stageContainer = container.querySelector("#stage");
        expect(stageContainer).toBeDefined();
        const canvas = stageContainer!.querySelector("canvas");
        expect(canvas).toBeDefined();
    });
});
