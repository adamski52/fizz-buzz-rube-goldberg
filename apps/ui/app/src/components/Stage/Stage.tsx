import React from "react";
import * as PIXI from "pixi.js";
import "./Stage.scss";
import SocketService, { ISocketMessage } from "../../services/SocketService";
import DisplayObject from "../DisplayObject/DisplayObject";

export interface IStageProps {
    socketService: SocketService;
    removeDelayMs: number;
    textSize: number;
}

export interface IStageState {};

export default class Stage extends React.Component<IStageProps, IStageState> {
    private app: PIXI.Application;
    private STAGE_ID = "stage";
    private items:DisplayObject[] = [];

    constructor(props: IStageProps) {
        super(props);
        this.app = new PIXI.Application({
            width: 500,
            height: 500,
            backgroundColor: 0x000000,
            // resolution: window.devicePixelRatio || 1
            resolution: 1
        });

        this.onMessageReceived = this.onMessageReceived.bind(this);
        this.props.socketService.registerCallback(this.onMessageReceived);
    }

    private onMessageReceived(message:ISocketMessage) {
        let textContainer = new DisplayObject({
            ...message,
            textSize: this.props.textSize
        }, this.items.length);

        this.items.push(textContainer);
        this.app.stage.addChild(textContainer);
        
        // setTimeout(() => {
        //     this.removeItem();
        // }, this.props.removeDelayMs);
    }

    private removeItem() {
        if(!this.items) {
            return;
        }

        let removedItem = this.items.shift();

        if(!removedItem) {
            return;
        }

        this.app.stage.removeChild(removedItem);
        
        this.items.forEach((item) => {
            item.moveUp();
        });
    }

    public render() {
        return (
            <div className="stage" id={this.STAGE_ID} />
        );
    }

    public componentDidMount() {
        document.getElementById(this.STAGE_ID)?.appendChild(this.app.view);
    }
}
