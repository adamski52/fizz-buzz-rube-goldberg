import DisplayObject from "./DisplayObject";
import PIXI from "pixi.js";

describe("DisplayObject", () => {

    let displayObject:DisplayObject;
    let child:PIXI.Text;
    let textSize = 50;
    let index = 1;
    let uuid = "wat";
    let message = "hello";

    beforeEach(() => {
        displayObject = new DisplayObject({
            message,
            uuid,
            textSize
        }, index);
        
        child = displayObject.getChildAt(0) as PIXI.Text;
    });

    it("should have the right defaults", () => {
        expect(child.text).toEqual(message);
        expect(child.style.fontSize).toEqual(textSize);
        expect(child.y).toEqual(textSize + (index * textSize));
        expect(child.x).toEqual(270);
    });

    it("should scoot up by text size when told to do so", () => {
        expect(child.y).toEqual(textSize + (index * textSize));
        displayObject.moveUp();
        expect(child.y).toEqual(index * textSize);
    });
});
