import * as PIXI from "pixi.js";

export interface IDisplayObjectProps {
    message: string;
    uuid: string;
    textSize: number;
}

export default class DisplayObject extends PIXI.Container {

    private text:PIXI.Text | undefined;
    private textSize:number = 20;
    private textStyle = new PIXI.TextStyle({
        fontFamily: "Roboto",
        dropShadow: true,
        dropShadowAlpha: 0.5,
        dropShadowAngle: 135,
        dropShadowBlur: 5,
        dropShadowColor: "0x000000",
        dropShadowDistance: 5,
        fill: ["#ffffff"],
        fontSize: this.textSize
    });

    
    constructor(props:IDisplayObjectProps, index:number) {
        super();

        this.textSize = props.textSize;
        this.textStyle.fontSize = this.textSize;

        this.text = new PIXI.Text(props.message, this.textStyle);
        this.text.anchor.set(0.5, 0.5);
        this.text.x = 270;
        this.text.y = props.textSize * (index+1);
        
        this.addChild(this.text);
    }

    public moveUp() {
        if(!this.text) {
            return;
        }

        this.text.y -= this.textSize;
    }
}
