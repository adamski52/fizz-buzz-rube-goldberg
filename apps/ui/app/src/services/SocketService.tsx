import { Socket} from "socket.io-client";

export interface ISocketMessage {
    uuid: string;
    message: string;
}

export interface ISocketRequestResponse {
    uuid: string;
}

export default class SocketService {
    private socket:Socket;
    private uuids:string[] = [];
    private callback:(message: ISocketMessage) => void = () => {};

    constructor(socket:Socket) {
        this.socket = socket;
    
        this.socket.on("toClient", (data:ISocketMessage) => {
            if(this.uuids.indexOf(data.uuid) < 0) {
                return;
            }

            this.callback(data);
        });
    }

    public keep(uuid: string) {
        this.uuids.push(uuid);
    }

    public registerCallback(callback: (message:ISocketMessage) => void) {
        this.callback = callback;
    }
}