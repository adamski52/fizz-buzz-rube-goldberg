import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./components/App/App";
import HttpService from "./services/HttpService";
import SocketService from "./services/SocketService";
import { io } from "socket.io-client";

const httpService = new HttpService();
const socketService = new SocketService(io());

ReactDOM.render(
    <React.StrictMode>
        <App httpService={httpService} socketService={socketService} removeDelayMs={10000} />
    </React.StrictMode>,
    document.getElementById("root")
);
