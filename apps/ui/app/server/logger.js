const winston = require("winston");

const LOGGER = winston.createLogger();

LOGGER.add(new winston.transports.Console({
    format: winston.format.json(),
    level: "debug"
}));

module.exports = LOGGER;
