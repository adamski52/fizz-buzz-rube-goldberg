const CONFIG = {
    HOST_PORT: process.env.HOST_PORT || 3000,
    PROXY_HOST: process.env.PROXY_HOST || "http://localhost:8080",
    SOCKET_HOST: process.env.SOCKET_HOST || "http://localhost:5000"
};

module.exports = CONFIG;
