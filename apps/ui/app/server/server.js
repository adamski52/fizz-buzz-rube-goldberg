const express = require("express");
const { createProxyMiddleware } = require("http-proxy-middleware");
const LOGGER = require("./logger");
const CONFIG = require("./config");

const app = express();

app.use(express.static("build"));

app.use("/api", createProxyMiddleware({
    target: CONFIG.PROXY_HOST,
    changeOrigin: true
}));

app.use("/socket.io", createProxyMiddleware({
    target: CONFIG.SOCKET_HOST
}));

app.use("/health", (req, res) => {
    LOGGER.info({
        message: "/health endpoint called"
    });
    res.status(200).send();
});

app.get("/*", (_req, res) => {
    res.sendFile(path.resolve(__dirname, "build/index.html"));
});

app.listen(CONFIG.HOST_PORT, () => {
    LOGGER.info({
        message: "Listening on: http://localhost:" + CONFIG.HOST_PORT,
        config: CONFIG
    });
});
