package handlers

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type FilesystemResponse []struct {
	Contents string `json:"contents"`
	UUID     string `json:"uuid"`
}

type AnswerRequest struct {
	Message string `json:"message"`
	UUID    string `json:"uuid"`
}

func DoDelete(url string) {
	log.Println("DELETE: " + url)
	// clobber file now that its handled
	req, err := http.NewRequest("DELETE", url, nil)
	womp(err)

	_, err = http.DefaultClient.Do(req)
	womp(err)
}

func DoPost(url string, payload AnswerRequest) {
	log.Println("POST: "+url, payload)

	jsonValue, _ := json.Marshal(payload)
	_, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
	womp(err)
}

func DoGet(url string) FilesystemResponse {
	log.Println("GET: " + url)

	// call the filesystem api
	res, err := http.Get(url)
	womp(err)

	// read it
	responseData, err := ioutil.ReadAll(res.Body)
	womp(err)

	// parse it into our type
	var parsedResponse FilesystemResponse
	err = json.Unmarshal([]byte(responseData), &parsedResponse)
	womp(err)

	return parsedResponse
}
