package handlers

import (
	"log"
	"os"
)

// cry if we get an error.  exit so the Job we're in reschedules itself.
func womp(err error) {
	if err != nil {
		log.Println(err.Error())
		os.Exit(1)
	}
}
