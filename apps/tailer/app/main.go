package main

import (
	"log"
	"os"

	"com.jonathanadamski/handlers"
)

func main() {
	FILESYSTEM_URL := os.Getenv("FILESYSTEM_URL")
	if len(FILESYSTEM_URL) == 0 {
		FILESYSTEM_URL = "http://localhost:8080/filesystem"
	}

	SOCKET_URL := os.Getenv("SOCKET_URL")
	if len(SOCKET_URL) == 0 {
		SOCKET_URL = "http://localhost:5000/fizzbuzz"
	}

	log.Println("FILESYSTEM_URL: " + FILESYSTEM_URL)
	log.Println("SOCKET_URL: " + SOCKET_URL)

	parsedResponse := handlers.DoGet(FILESYSTEM_URL)

	for _, val := range parsedResponse {
		log.Println("Handling item: ", val)

		handlers.DoPost(SOCKET_URL, handlers.AnswerRequest{
			UUID:    val.UUID,
			Message: val.Contents,
		})
		handlers.DoDelete(FILESYSTEM_URL + "/" + val.UUID)
	}
}
