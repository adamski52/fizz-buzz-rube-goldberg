# Rube Goldberg Fizz Buzz

---

## Note

This is an intentionally overcomplicated answer to a simple problem.  Please do not judge my problem solving by this.  It's purposefully a bad solution in order to exercise different technologies.  I'd never make something so complicated for such a problem "in real life."

#### Who's Rube Goldberg?
A cartoonist who popularized the idea of making elaborate, complicated contraptions in order to accomplish the simplest of tasks.

* Learn learn more about him:  https://en.wikipedia.org/wiki/Rube_Goldberg
* Learn more about the genre of contraptions named after him: https://en.wikipedia.org/wiki/Rube_Goldberg_machine

#### What's Fizz Buzz?
Fizz Buzz is a textbook interview question.  Its problem statement is easy to understand and its solution is easy to execute.  It allows the interviewer to quickly gauge the developer's problem solving/programming abilities.  It's one of those things that has an obvious answer but also a billion more sophisticated, harder to understand (but technically suprior) answers.

The very basic idea is that the program should:
* Count from 0 to N
* For every number that's divisible by 3, print "Fizz"
* For every number that's divisible by 5, "Buzz"
* For any other number, print the number.

For example:
```
`0, 1, 2, Fizz, 4, Buzz, Fizz, 7...`
```

* Learn more about Fizz Buzz: https://en.wikipedia.org/wiki/Fizz_buzz

---


## OK, so tell me what it's about already...
I am of the belief that the most challenging part of a developer's career is keeping up with changing technology.  As I get more specialized in my career, I find that I have less time to actually use all of the fun new technology released (unless my position at the time happens to have a need for it).  To help keep myself relevant, I started this project to demonstrate these technologies and how I handle adding features to a legacy system.

Development is often about trying to find the simplest solution to a complicated problem.  Rube Goldberg Machines are about trying to find the most complicated solution to the simplest problem.  Do you see where this is going?

The manner in which I'm doing this is, of course, silly:  **I'd never seriously try to plumb all of this stuff together just to do Fizz Buzz**.  But often, especially at startups who are growing up or at very large coporations, you find yourself working with a rather simple system, all things considered, with years of duct tape holding it together.  Legacy systems are, essentially, accidental Rube Goldberg machines.

So, let's see how complicated I can make Fizz Buzz.



## What are all the parts / order of operations?
![All of this](thumbnail.jpg "All of this")


## What does this do?
It's easier to watch me explain it.  The "messaages" typo on the recording diagram will haunt me for life though.
![Demo](demo.mp4)
